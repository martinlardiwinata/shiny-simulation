overview_page2_UI <- function (){
  tags$div (id = "overview_setupPage_box_css",box (width = 12, 
     column (8,
             textInput (inputId = "rx_syntax",
                        label = "RX Syntax",
                        value = "150 mg over 0.25 in 2, 100 mg in 1 over 0.25 q14 x 6 after 28, 100 mg over 0.25 in 2 q7 x 8 after 0.5")),
     column (2,actionButton("rx_syntax_docs_bttn", "",icon = icon ("info")),
             actionButton("submit_rx_syntax", "Review")),
             overviewPage2UI ("overviewPage2_ModID"),
             actionButton("switch_page_1","Save Simulation"),
     bsModal(id = "rx_syntax_documentation",title = "Rx Dosing Docs",trigger = "rx_syntax_docs_bttn",
             fluidRow(column = 12, includeMarkdown("www/mrgsolve_ev_rx_docs.rmd")), size = "large")
     
  ))
}